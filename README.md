# ImageSteganography

This program can hide an image inside of another image steganographically.
This is done by replacing the n least significant bits of each color component of every pixel in the original image with the n most significant bits of the hidden image.