module Main (main) where

import Codec.Picture
import Data.Bits
import Data.Word
import Data.Vector.Storable

main :: IO ()
main = do
    -- Inserting secret image
    img1 <- readImage "img1.png"
    img2 <- readImage "img2.png"
    case img1 of
        Left s -> putStrLn s
        Right (ImageRGB8 i1) -> case img2 of
            Left s -> putStrLn s
            Right (ImageRGB8 i2) -> savePngImage "inserted.png" $ ImageRGB8 $ liftI2 insert i1 i2
            Right _ -> onlyRGB8
        Right _ -> onlyRGB8

    -- Extracting secret image
    img <- readImage "inserted.png"
    case img of
        Left s -> putStrLn s
        Right (ImageRGB8 i) -> savePngImage "extracted.png" $ ImageRGB8 $ pixelMap extract i
        Right _ -> onlyRGB8

insignificantBits :: Int
insignificantBits = 2

liftI2 :: (Pixel a, Pixel b, Pixel c) => (a -> b -> c) -> Image a -> Image b -> Image c
liftI2 f img1@Image{imageWidth = w, imageHeight = h} img2 =
    generateImage (\x y -> f (pixelAt img1 x y) $ pixelAt img2 x y) w h

insert :: PixelRGB8 -> PixelRGB8 -> PixelRGB8
insert (PixelRGB8 r1 g1 b1) (PixelRGB8 r2 g2 b2) = PixelRGB8 nr ng nb
                               where [nr,ng,nb] = Prelude.zipWith (combineBits insignificantBits) [r1,g1,b1] [r2,g2,b2]

extract :: PixelRGB8 -> PixelRGB8
extract (PixelRGB8 r g b) = PixelRGB8 nr ng nb
                               where [nr,ng,nb] = fmap (leastSignificantBits insignificantBits) [r,g,b]

leastSignificantBits :: Bits a => Int -> a -> a
leastSignificantBits n = flip shiftL (8 - n)

combineBits :: Bits a => Int -> a -> a -> a
combineBits n b1 b2 = (.|.) (shiftR b2 (8 - n)) $ (.&.) b1 $ shiftL Main.oneBits n

onlyRGB8 :: IO ()
onlyRGB8 = putStrLn "Only RGB8 images are supported."

-- Constraints on Data.Bits.oneBits are too strong. Define our own.
oneBits :: Bits a => a
oneBits = complement zeroBits